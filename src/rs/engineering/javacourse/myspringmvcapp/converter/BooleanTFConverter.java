package rs.engineering.javacourse.myspringmvcapp.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
@Converter
public class BooleanTFConverter implements AttributeConverter<Boolean, String> {
	@Override
    public String convertToDatabaseColumn(Boolean value) {
        if (Boolean.TRUE.equals(value)) {
            return "YES";
        } else {
            return "NO";
        }
    }
    @Override
    public Boolean convertToEntityAttribute(String value) {
        return "YES".equals(value);
    }
}
