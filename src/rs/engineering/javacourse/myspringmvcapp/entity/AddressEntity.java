
package rs.engineering.javacourse.myspringmvcapp.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class AddressEntity implements Serializable{

	private static final long serialVersionUID = -572836044622957515L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String street;
	private String streetNumber;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CITY_ID")
	private CityEntity cityEntity;
	public AddressEntity() {
		cityEntity = new CityEntity();
	}
	
	public AddressEntity(String street, String streetNumber, CityEntity cityEntity) {
		super();
		this.street = street;
		this.streetNumber = streetNumber;
		this.cityEntity = cityEntity;
	}

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public CityEntity getCityEntity() {
		return cityEntity;
	}
	public void setCityEntity(CityEntity cityEntity) {
		this.cityEntity = cityEntity;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "AddressEntity [street=" + street + ", streetNumber=" + streetNumber + ", cityEntity=" + cityEntity
				+ "]";
	}
	
	
}
