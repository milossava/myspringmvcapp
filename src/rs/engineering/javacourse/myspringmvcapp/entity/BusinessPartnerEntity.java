
package rs.engineering.javacourse.myspringmvcapp.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import rs.engineering.javacourse.myspringmvcapp.converter.BooleanTFConverter;

@Entity
@Table(name = "business_partner")
public class BusinessPartnerEntity implements Serializable {

	private static final long serialVersionUID = 355088178282267037L;
	
	@Id
	@Column(name = "IDENTIFICATION_NUMBER")
	private String identificationNumber;
	private String name;
	@OneToOne(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name = "ADDRESS_ID")
	private AddressEntity addressEntity;
	@Column(name = "TAX_PAYER")
	@Convert(converter=BooleanTFConverter.class)
	private boolean taxPayer;
	@Column(name = "TAX_NUMBER")
	private String taxNumber;

	public BusinessPartnerEntity() {
		addressEntity = new AddressEntity();
	}
	
	public BusinessPartnerEntity(String identificationNumber, String name, AddressEntity addressEntity,
			boolean taxPayer, String taxNumber) {
		super();
		this.identificationNumber = identificationNumber;
		this.name = name;
		this.addressEntity = addressEntity;
		this.taxPayer = taxPayer;
		this.taxNumber = taxNumber;
	}



	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AddressEntity getAddressEntity() {
		return addressEntity;
	}

	public void setAddressEntity(AddressEntity addressEntity) {
		this.addressEntity = addressEntity;
	}

	public boolean isTaxPayer() {
		return taxPayer;
	}

	public void setTaxPayer(boolean taxPayer) {
		this.taxPayer = taxPayer;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	@Override
	public String toString() {
		return "BusinessPartnerEntity [identificationNumber=" + identificationNumber + ", name=" + name
				+ ", addressEntity=" + addressEntity + ", taxPayer=" + taxPayer + ", taxNumber=" + taxNumber + "]";
	}
	
	
}
