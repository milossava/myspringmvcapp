package rs.engineering.javacourse.myspringmvcapp.exception;

public class MyValidationException extends Exception{
	private static final long serialVersionUID = -2332335886808719578L;

	public MyValidationException(String message) {
		super(message);
	}
}
