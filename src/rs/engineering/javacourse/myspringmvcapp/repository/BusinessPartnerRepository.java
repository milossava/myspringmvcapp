package rs.engineering.javacourse.myspringmvcapp.repository;

public interface BusinessPartnerRepository<T> {
	void save(T businessPartner);
}
