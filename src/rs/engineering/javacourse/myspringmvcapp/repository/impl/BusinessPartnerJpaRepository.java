package rs.engineering.javacourse.myspringmvcapp.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import rs.engineering.javacourse.myspringmvcapp.entity.BusinessPartnerEntity;
import rs.engineering.javacourse.myspringmvcapp.repository.BusinessPartnerRepository;

@Repository(value = "businessPartnerJpaRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class BusinessPartnerJpaRepository implements BusinessPartnerRepository<BusinessPartnerEntity>{
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public void save(BusinessPartnerEntity businessPartner) {
		entityManager.persist(businessPartner);
		
	}

}
