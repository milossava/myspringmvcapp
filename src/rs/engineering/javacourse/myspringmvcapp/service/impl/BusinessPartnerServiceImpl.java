package rs.engineering.javacourse.myspringmvcapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.engineering.javacourse.myspringmvcapp.entity.AddressEntity;
import rs.engineering.javacourse.myspringmvcapp.entity.BusinessPartnerEntity;
import rs.engineering.javacourse.myspringmvcapp.entity.CityEntity;
import rs.engineering.javacourse.myspringmvcapp.exception.MyValidationException;
import rs.engineering.javacourse.myspringmvcapp.model.BusinessPartnerDto;
import rs.engineering.javacourse.myspringmvcapp.repository.BusinessPartnerRepository;
import rs.engineering.javacourse.myspringmvcapp.service.BusinessPartnerService;
@Service
@Transactional
public class BusinessPartnerServiceImpl implements BusinessPartnerService{
	@Qualifier(value = "businessPartnerJpaRepository")
	@Autowired
	private BusinessPartnerRepository<BusinessPartnerEntity> businessPartnerRepository;

	
	@Override
	public void save(BusinessPartnerDto businessPartnerDto) throws MyValidationException {
		if(businessPartnerDto.getIdentificationNumber().equals("i1")) {
			throw new MyValidationException("Business partner already exist");
		}
		if(businessPartnerDto.getIdentificationNumber().equals("null")) {
			throw new NullPointerException("Null pointer exception.");
		}
		CityEntity cityEntity = new CityEntity(businessPartnerDto.getAddressDto().getCityDto().getNumber(),
				businessPartnerDto.getAddressDto().getCityDto().getName());
		AddressEntity addressEntity = new AddressEntity(businessPartnerDto.getAddressDto().getStreet(),
				businessPartnerDto.getAddressDto().getStreetNumber(), cityEntity);
		BusinessPartnerEntity businessPartnerEntity = new BusinessPartnerEntity(
				businessPartnerDto.getIdentificationNumber(), businessPartnerDto.getName(),
				addressEntity, businessPartnerDto.isTaxPayer(), businessPartnerDto.getTaxNumber());
		businessPartnerRepository.save(businessPartnerEntity);
	}

}
