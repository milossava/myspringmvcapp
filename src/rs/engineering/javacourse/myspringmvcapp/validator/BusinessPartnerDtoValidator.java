package rs.engineering.javacourse.myspringmvcapp.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import rs.engineering.javacourse.myspringmvcapp.model.BusinessPartnerDto;

public class BusinessPartnerDtoValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return BusinessPartnerDto.class.equals(clazz);
	}

	@Override//nece se izvrsiti ako gore u metodi supports nije klasa BusinessPartnerDto
	public void validate(Object target, Errors errors) {
		BusinessPartnerDto businessPartnerDto = (BusinessPartnerDto)target;
		System.out.println("======BusinessPartnerDtoValidator=======");
		System.out.println(businessPartnerDto);
		System.out.println("=============");
		
		if(businessPartnerDto!=null && businessPartnerDto.isTaxPayer()) {
			//mora da ima pib
			if(businessPartnerDto.getTaxNumber().isEmpty()) {
				errors.rejectValue("taxNumber", "businessPartner.taxNumber", 
						"You must have tax number for tax payer business partner!");
			}
		}
	}

}
