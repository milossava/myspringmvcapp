<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Global Exception</title>
</head>
<body>
	<p>My global exception page...</p>
	<p>
		Error object: ${errorObj}
	</p>
	<p>
		Error message: ${errorMessage}
	</p>
</body>
</html>